package org.orangee.documentmanager.services;

import org.apache.commons.io.FilenameUtils;
import org.orangee.documentmanager.models.entities.Document;
import org.orangee.documentmanager.repositories.DocumentRepository;
import org.orangee.documentmanager.services.intf.DocumentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import javax.transaction.Transactional;
import java.io.IOException;
import java.util.List;

@Service
public class DocumentServiceImpl implements DocumentService {

    private DocumentRepository repository;


    @Transactional
    public Document store(Long ownerId, String name,  String filename, String docType, byte[] file) throws IOException {
        Document document = new Document(docType, name, FilenameUtils.getExtension(filename), filename, file, ownerId);

        return repository.save(document);
    }

    @Override
    public Document retrieveById(Long documentId) {
        return repository.findById(documentId).orElse(null);
    }

    @Override
    public List<Document> retrieveDocumentsByOwnerId(Long ownerId) {
        return repository.findAllDocumentsByOwner(ownerId);
    }


    @Autowired
    public void setRepository(DocumentRepository repository) {
        this.repository = repository;
    }
}
