package org.orangee.documentmanager.services.converters;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;
import org.orangee.documentmanager.models.dtos.DocumentDTO;
import org.orangee.documentmanager.models.entities.Document;

import java.util.List;

@Mapper(componentModel = "spring", uses= { })
public interface DocumentMapper {

    DocumentMapper INSTANCE = Mappers.getMapper(DocumentMapper.class);

    @Mappings({
            @Mapping(source="archive", target = "file", ignore = true)
    })
    DocumentDTO toDTO(Document entity);

    List<DocumentDTO> toDTOs(List<Document> entity);
}
