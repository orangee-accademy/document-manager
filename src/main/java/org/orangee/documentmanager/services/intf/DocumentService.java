package org.orangee.documentmanager.services.intf;

import org.orangee.documentmanager.models.entities.Document;

import java.io.IOException;
import java.util.List;

public interface DocumentService {
    Document store(Long ownerId, String name, String filename, String docType, byte[] file) throws IOException;

    Document retrieveById(Long documentId);

    List<Document> retrieveDocumentsByOwnerId(Long ownerId);
}
