package org.orangee.documentmanager.models.dtos;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.ArrayList;
import java.util.Collection;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class ResponseDTO<D> {

    private D data;
    private Collection<SimpleMessageDTO> messages = new ArrayList<>();

    public static <D> ResponseDTO<D> createResponseDTO(D dto) {
        ResponseDTO<D> response = new ResponseDTO<>();
        response.setData(dto);
        return response;
    }

    public D getData() {
        return data;
    }

    public void setData(D data) {
        this.data = data;
    }

    public Collection<SimpleMessageDTO> getMessages() {
        return messages;
    }

    public void setMessages(Collection<SimpleMessageDTO> messages) {
        this.messages = messages;
    }
}
