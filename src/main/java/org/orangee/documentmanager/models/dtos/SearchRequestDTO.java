package org.orangee.documentmanager.models.dtos;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter @Setter @NoArgsConstructor
public class SearchRequestDTO {

    private Long id;
    private String name;
    private String email;
    private String phoneNumber;
    private String code;
    private Integer month;
    private Integer year;
    private Integer pageNo;
    private Integer pageSize;
    private String sortBy;

    public SearchRequestDTO(Long id, String name, String email, String phoneNumber, Integer pageNo, Integer pageSize, String sortBy) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.phoneNumber = phoneNumber;
        this.pageNo = pageNo;
        this.pageSize = pageSize;
        this.sortBy = sortBy;
    }

    public SearchRequestDTO(Long id, String name, String code, Integer month, Integer year, Integer pageNo, Integer pageSize, String sortBy) {
        this.id = id;
        this.name = name;
        this.code = code;
        this.month = month;
        this.year = year;
        this.pageNo = pageNo;
        this.pageSize = pageSize;
        this.sortBy = sortBy;
    }
}
