package org.orangee.documentmanager.models.dtos;

import com.sun.istack.NotNull;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.web.multipart.MultipartFile;

import java.time.LocalDateTime;

@Getter @Setter @NoArgsConstructor
public class DocumentDTO {

    @NotNull
    private Long ownerId;
    @NotNull
    private String name;
    @NotNull
    private String docType;
    private String filename;

    private String file;
    private String uri;
    private byte[] fileContent;

    private LocalDateTime modifiedDate;
    private LocalDateTime dateCreated;

    public DocumentDTO(String name, String docType, String filename) {
        this.name = name;
        this.docType = docType;
        this.filename = filename;
    }

    public DocumentDTO(String name, String docType, String filename, LocalDateTime dateCreated, LocalDateTime modifiedDate) {
        this.name = name;
        this.docType = docType;
        this.filename = filename;
    }
}
