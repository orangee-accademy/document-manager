package org.orangee.documentmanager.models.dtos;

public enum ResponseMessageType {
    WARNING,
    ERROR,
    INFO,
    AUTH
}
