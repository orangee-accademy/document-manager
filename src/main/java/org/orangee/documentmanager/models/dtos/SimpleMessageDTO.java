package org.orangee.documentmanager.models.dtos;


import org.springframework.http.HttpStatus;

public class SimpleMessageDTO {
    private String message;
    private String target;
    private HttpStatus status;
    private ResponseMessageType type;

    public SimpleMessageDTO() {

    }

    public SimpleMessageDTO(String message, String target, HttpStatus status, ResponseMessageType type) {
        if (message==null && status!=null)
            message = status.getReasonPhrase();
        this.message = message;
        this.target = target;
        this.status = status;
        this.type = type;
        if (type==null)
            this.type = ResponseMessageType.INFO;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        this.target = target;
    }

    public HttpStatus getStatus() {
        return status;
    }

    public ResponseMessageType getType() {
        return type;
    }

    public void setType(ResponseMessageType type) {
        this.type = type;
    }
}
