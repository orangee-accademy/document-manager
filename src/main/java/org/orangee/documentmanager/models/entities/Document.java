package org.orangee.documentmanager.models.entities;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.Table;

@Entity
@Table(name = "documents")
@Getter @Setter @NoArgsConstructor @AllArgsConstructor
public class Document extends AbstractBaseEntity {

    @Column(name = "doc_type")
    private String docType;
    @Column(name = "name")
    private String name;
    private String fileType;
    @Column(name = "filename")
    private String filename;
    @Lob
    @Column(name = "archive")
    private byte[] archive;
    @Column(name = "owner_id")
    private Long ownerId;
}
