package org.orangee.documentmanager.repositories;

import org.orangee.documentmanager.models.entities.Document;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DocumentRepository extends JpaRepository<Document, Long> {

    @Query("Select d from Document d where d.ownerId=:ownerId order by d.dateCreated asc")
    List<Document> findAllDocumentsByOwner(Long ownerId);
}
