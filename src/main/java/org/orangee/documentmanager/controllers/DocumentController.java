package org.orangee.documentmanager.controllers;

import org.orangee.documentmanager.models.dtos.DocumentDTO;
import org.orangee.documentmanager.models.dtos.ResponseDTO;
import org.orangee.documentmanager.models.entities.Document;
import org.orangee.documentmanager.services.converters.DocumentMapper;
import org.orangee.documentmanager.services.intf.DocumentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.servlet.http.HttpServletRequest;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/documents")
public class DocumentController {

    private DocumentService documentService;
    private DocumentMapper documentMapper;


    @GetMapping("/{documentId}")
    public ResponseEntity<ResponseDTO<DocumentDTO>> getDocument(@PathVariable("documentId") Long id,
                                                                HttpServletRequest req) {
        ResponseDTO<DocumentDTO> response = new ResponseDTO<>();
        Document document = documentService.retrieveById(id);
        if (document==null) {
            return ResponseEntity.notFound().build();
        }
        DocumentDTO dto = documentMapper.toDTO(document);
        dto.setUri(ServletUriComponentsBuilder.fromCurrentContextPath().build().toUriString().concat("/documents/download/id/").concat(""+document.getId()));
        response.setData(dto);
        return new ResponseEntity(response, HttpStatus.OK);
    }


    @GetMapping("/owner/{ownerId}")
    public ResponseEntity<ResponseDTO<List<DocumentDTO>>> getDocumentsByOwner(@PathVariable("ownerId") Long ownerId,
                                                                        HttpServletRequest req) {
        ResponseDTO<List<DocumentDTO>> response = new ResponseDTO<>();
        List<Document> documents = documentService.retrieveDocumentsByOwnerId(ownerId);
        if (documents==null) {
            return ResponseEntity.notFound().build();
        }
        List<DocumentDTO> dtos = new ArrayList<>();
        for(Document doc: documents) {
            DocumentDTO dto = documentMapper.toDTO(doc);
            dto.setUri(ServletUriComponentsBuilder.fromCurrentContextPath().build().toUriString().concat("/documents/download/id/").concat("" + doc.getId()));
            dtos.add(dto);
        }
        response.setData(dtos);
        return new ResponseEntity(response, HttpStatus.OK);
    }

    @GetMapping(value = "/download/id/{documentId}")
    public ResponseEntity<Resource> downloadFile(@PathVariable("documentId") Long id) {
        Document document = documentService.retrieveById(id);
        return ResponseEntity.ok()
                .contentType(MediaType.parseMediaType(URLConnection.getFileNameMap().getContentTypeFor(document.getFilename())))
                .header(HttpHeaders.CONTENT_DISPOSITION,
                        "attachment; filename=\"" + document.getFilename() + "\"")
                .body(new ByteArrayResource(document.getArchive()));
    }

    @PostMapping
    public ResponseEntity<ResponseDTO> upload(@RequestBody DocumentDTO dto) {
        ResponseDTO response = new ResponseDTO();
        try {
            documentService.store(dto.getOwnerId(), dto.getName(), dto.getFilename(), dto.getDocType(), dto.getFileContent());
        } catch (Exception e) {
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity(response, HttpStatus.OK);
    }


    @Autowired
    public void setDocumentService(DocumentService documentService) {
        this.documentService = documentService;
    }
    @Autowired
    public void setDocumentMapper(DocumentMapper documentMapper) {
        this.documentMapper = documentMapper;
    }
}
