package org.orangee.documentmanager.configurations;



import org.springframework.data.domain.AuditorAware;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class SpringSecurityAuditorAware implements AuditorAware<String> {

    @Override
    public Optional<String> getCurrentAuditor() {

        /*
         * FIXME Al momento della registrazione non esiste un utente loggato per cui lo username sarà sempre null
         * provocando un NullPointerException.
         * Per il momento viene settata una mail di default (trovare un fix migliore)
         */
        final String defaultUser = "system";
        return Optional.of(defaultUser);
    }
}
